# Simgrid's Comm async bug

This MWE shows that async comms don't behave as they used to (i.e. crash sometimes)

To build and test, simply

```
cd build 
cmake ..
make
./test ../platform.xml ../deploy.xml
```

Example output is shown for "good" commit 756302d2 and "bad" master (96d81588) in files 756302d2.log and master.log

The bug was introduced by commit b1020c2311048f26a586af241101434faa59445b :

```
git bisect start
# bad: [96d81588155dceb9c433e3bcb2a8a688036bffb2] pimpl_ will never be null.
git bisect bad 96d81588155dceb9c433e3bcb2a8a688036bffb2
# good: [756302d2bdd3f22465e90d820fa526bbcc08a9fe] Remove obsolete entries in .gitignore.
git bisect good 756302d2bdd3f22465e90d820fa526bbcc08a9fe
# bad: [10223740bfb5c2ebf8247e1fb646cfe7fee5749e] Kill ancient compatibility hack (fixed since elfutils 0.155 in 2012).
git bisect bad 10223740bfb5c2ebf8247e1fb646cfe7fee5749e
# bad: [9b89e098cd45d4517c6ffac422997278f9bedc34] Rename an example: I'm always looking for a simple comm example, so here it is
git bisect bad 9b89e098cd45d4517c6ffac422997278f9bedc34
# good: [97d71d1ac0bb8eb57e82c604b74ae517de2a26cc] doing a rename
git bisect good 97d71d1ac0bb8eb57e82c604b74ae517de2a26cc
# bad: [450b524c743d19378a2efa1d3c19672e19fa4b2c] Merge with fragmagit/master
git bisect bad 450b524c743d19378a2efa1d3c19672e19fa4b2c
# bad: [b1020c2311048f26a586af241101434faa59445b] New functions: Comm::sendto_{init,async}
git bisect bad b1020c2311048f26a586af241101434faa59445b
# good: [765fec3aaea82e478ef7c534dc7867c6c13aa14d] Remove a redundent line (was already given 6 lines above)
git bisect good 765fec3aaea82e478ef7c534dc7867c6c13aa14d
# first bad commit: [b1020c2311048f26a586af241101434faa59445b] New functions: Comm::sendto_{init,async}
```