
#include <simgrid/s4u.hpp>

XBT_LOG_NEW_DEFAULT_CATEGORY(s4u_app_masterworker, "Messages specific for this example");

using namespace simgrid;

static void sender(std::vector<std::string> args)
{
  xbt_assert(args.size() == 1, "No argument");

  s4u::Actor::self()->get_host()->sendto_async(s4u::Host::by_name("Jupiter"), 1e3);

  XBT_INFO("Exiting now.");
}

int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n", argv[0]);

  e.register_function("sender", &sender);

  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);

  e.run();

  XBT_INFO("Simulation is over");

  return 0;
}
